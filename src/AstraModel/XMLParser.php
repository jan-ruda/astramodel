<?php
namespace MyApp\AstraModel;

/**
 * Class XMLParser
 * @package MyApp\AstraModel
 */
class XMLParser
{
    /**
     * @var bool $downloaded
     */
    private $downloaded;

    /**
     * @var bool $decompressed
     */
    private $decompressed;

    /**
     * @var string $url
     */
    private $url;

    /**
     * @var int $timeout
     */
    private $timeout;

    /**
     * @var string $tempDirectory
     */
    private $tempDirectory;

    /**
     * @var string $downloadFileName
     */
    private $downloadFileName;

    /**
     * @var string $xmlName
     */
    private $xmlName;


    /**
     * XMLParser constructor.
     * @param string $url
     * @param string $tempDirectory
     * @param string $xmlName
     * @param int $timeout
     */
    public function __construct($url, $tempDirectory, $xmlName, $timeout = 30)
    {
        $this->downloaded = false;
        $this->decompressed = false;
        $this->timeout = $timeout;
        $this->url = $url;
        $this->tempDirectory = $tempDirectory;
        $this->downloadFileName = $this->tempDirectory.'download.zip';
        $this->xmlName = $tempDirectory.$xmlName;
    }

    /**
     * Is the file downloaded?
     * @return bool
     */
    public function isDownloaded()
    {
        return $this->downloaded;
    }

    /**
     * Is the downloaded file decompressed?
     * @return bool
     */
    public function isDecompressed()
    {
        return $this->decompressed;
    }

    /**
     * Get url
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get tempDirectory
     * @return string
     */
    public function getTempDirectory()
    {
        return $this->tempDirectory;
    }

    /**
     * Get downloadFileName
     * @return string
     */
    public function getDownloadFileName()
    {
        return $this->downloadFileName;
    }

    /**
     * Get xmlName
     * @return string
     */
    public function getXmlName()
    {
        return $this->xmlName;
    }

    /**
     * Download zip file from url
     * @return bool
     * @throws \Exception
     */
    public function download()
    {
        $downloadResource = fopen($this->downloadFileName, "w");
        if ($downloadResource === false) {
            throw new \Exception('Can not create a file: ' . $this->downloadFileName);
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_FILE, $downloadResource);
        curl_exec($curl);
        if (curl_errno($curl)) {
            throw new \Exception(curl_error($curl));
        }
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $this->downloaded = $statusCode == 200?true:false;
        return $this->downloaded;
    }


    /**
     * Decompressing the downloaded file
     * @return bool
     * @throws \Exception
     */
    public function decompress()
    {
        if (!$this->isDownloaded()) {
            throw new \Exception('Call the download method first!');
        }
        $zip = new \ZipArchive;
        $res = $zip->open($this->downloadFileName);
        if ($res === true) {
            $zip->extractTo($this->tempDirectory);
            $zip->close();
            $this->decompressed = true;
        } else {
            throw new \Exception('Error decompressing the downloaded file.');
        }
        return $this->decompressed;
    }

    /**
     * Parsing XML file and return array of result
     * @return array
     */
    public function parseXML()
    {
        $result = [];
        $reader = new \XMLReader();
        $reader->open($this->xmlName);
        while ($reader->read()) {
            if ($reader->nodeType == \XMLReader::ELEMENT && $reader->name == 'item' && $reader->depth == 2) {
                $result[$reader->getAttribute('code')] = [
                    'code' => $reader->getAttribute('code'),
                    'name' => $reader->getAttribute('name'),
                ];
                if (!$reader->isEmptyElement) {
                    $item = new \SimpleXMLElement($reader->readOuterXml());
                    foreach ($item->children()->parts as $parts) {
                        if (count($parts) > 0) {
                            foreach ($parts as $part) {
                                if ($part->attributes()->name == "Náhradní díly") {
                                    $spareParts = [];
                                    foreach ($part->children()->item as $item) {
                                        $spareParts[(string)$item->attributes()->code] = (string)$item->attributes()->name;
                                    }
                                    if (count($spareParts) > 0) {
                                        $result[$reader->getAttribute('code')]['spareParts'] = $spareParts;
                                    }
                                    unset($spareParts);
                                }
                            }
                        }
                    }
                } else {
                    $result[$reader->getAttribute('code')]['canceled'] = true;
                }
            }
        }
        return $result;
    }
}
