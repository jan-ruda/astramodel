<?php
namespace MyApp\Tests\AstraModel;

use MyApp\AstraModel\XMLParser;
use PHPUnit\Framework\TestCase;

class XMLParserTest extends TestCase
{
    /**
     * @var XMLParser
     */
    protected $xmlParser;

    public function setUp()
    {
        $this->xmlParser = new XMLParser('http://www.astramodel.cz/b2b/export_xml_PSs6t5prnOYaHfTOUI-6XNF6m.zip',__DIR__.'/../../tmp/','export_full.xml',60);
    }

    public function testTempDirectory()
    {
        $this->assertDirectoryExists($this->xmlParser->getTempDirectory());
        $this->assertDirectoryIsWritable($this->xmlParser->getTempDirectory());
    }


    public function testDownloadAndDecompress()
    {
        $this->xmlParser->download();
        $this->xmlParser->decompress();
        $this->assertTrue($this->xmlParser->isDownloaded());
        $this->assertFileExists($this->xmlParser->getDownloadFileName());
        $this->assertTrue($this->xmlParser->isDecompressed());
        $this->assertFileExists($this->xmlParser->getXmlName());

    }

    public function testParsingXml()
    {
        $result = $this->xmlParser->parseXML();
        $this->assertNotCount(0,$result);
        $sparePartsCount = 0;
        $canceledCount = 0;
        foreach ($result as $item) {
            $this->assertArrayHasKey('code',$item);
            $this->assertArrayHasKey('name',$item);
            if (array_key_exists('spareParts',$item)) $sparePartsCount++;
            if (array_key_exists('canceled',$item)) $canceledCount++;
        }
        $this->assertGreaterThan(0,$sparePartsCount);
        $this->assertLessThanOrEqual(count($result),$sparePartsCount);
        $this->assertGreaterThan(0,$canceledCount);
    }




}