<?php

require_once __DIR__ . '/vendor/autoload.php';

use MyApp\AstraModel\XMLParser;
use SebastianBergmann\Timer\Timer;

echo "Astra Model XML Parser\n";
echo "Start timer - ";
Timer::start();
print Timer::resourceUsage();
echo "\n";

/**
 * @var XMLParser
 */
$astraModel = new XMLParser('http://www.astramodel.cz/b2b/export_xml_PSs6t5prnOYaHfTOUI-6XNF6m.zip', __DIR__.'/tmp/', 'export_full.xml', 60);
try {
    echo "Start downloading ... ";

    $astraModel->download();
    echo "done - ";
    print Timer::resourceUsage();
    echo "\n";
    echo "Start decompressing ... ";
    $astraModel->decompress();
    echo "done - ";
    print Timer::resourceUsage();
    echo "\n";
    echo "Start parsing ... ";
    $result = $astraModel->parseXML();

    echo "done - ";
    print Timer::resourceUsage();
    echo "\n";
} catch (Exception $e) {
    echo 'Error: ',  $e->getMessage(), "\n";
    if (isset($_SERVER['REQUEST_METHOD'])) {
        http_response_code(500);
    }
}

if (isset($result)) {
    echo (string)count($result)." items found\n";
    echo "Saving result for jQuery DataTable (json file) ... ";
    $jsonData = [];
    foreach ($result as $key=>$item) {
        $jsonData['data'][] = $item;
    }
    $jsonFile = fopen('results.json', 'w');
    fwrite($jsonFile, json_encode($jsonData, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT));
    unset($result);
    fclose($jsonFile);
    echo "done - ";
    print Timer::resourceUsage();
    echo "\n";
}
Timer::stop();
print Timer::resourceUsage();
echo "\n";