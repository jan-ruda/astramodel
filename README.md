# Astra Model XML Parser
## Zadání
Skript by po zavolání měl vypsat:
1) kolik je tam produktů 
2) názvy produktů vypíše (prostě jeden za druhým pod sebou, nemusíte řešit žádné formátování apod...)
3) Vypíšete k jednotlivým produktům náhradní díly (jsou jen u některých)

PHP má funkce pro práci s ZIP, soubor stáhnete pomocí např. CURL, pro průchod můžete použít klasické XML techniky (sax, parsery apod.)

index.php může vypadat třeba tak, že bude možné vybrat tři metody - odkazy (podle čísla toho úkolu) a pod každým odkazem budou ty jednotlivé výpisy... 
## Instalace
```
git clone https://gitlab.com/jan-ruda/astramodel.git
cd astramodel
composer update
```
## Skript pro stažení, rozbalení a přečtení XML souboru
Tento skript provede stažení zip souboru, rozbalení, parsování XML a vypisuje průběžný čas mezi operacemi a využití paměti.
Nakonec vypíše počet položek (bod č.1) a připraví soubor results.json, který je použit pro zobrazení (bod č.2 a č.3).
 
### Spuštění skriptu z příkazové řádky

```
php cron.php
```
Po spuštění:

![cron](images/cron.png)

### Spuštění z webového rozhraní

Skript lze spustit také z webového rozhraní. Zde je volán ajaxem a při ušpěsném provedení scriptu je aktualizován datový zdroj DataTables pluginu.

![button](images/button.png)

## Zobrazení výpisu produktu (bod č.1, č.2 a č.3)
Pro přehlednost jsem použil jQuery plugin DataTables. Položky, které obsahují náhradní díly, mají před sebou ikonu +
a jdou rozbalit. Zrušené položky mají červené pozadí a přeškrtnutý text.

### Spuštění lokálního serveru
```
php -S localhost:8000
```
Na adrese [http://localhost:8000](http://localhost:8000) je v tabulce zobrazen výpis všech položek

![web](images/web.png)
## Testy
### Statická anlýza kódu (pomocí PHPStan)
```
php vendor/bin/phpstan analyse --autoload-file=/vendor/autoload.php src -l max
```
Po spuštění:

![phpstan](images/phpstan.png)
### Testy (PHPUnit)
```
php vendor/bin/phpunit --testdox
```
Po spuštění:

![phpunit](images/phpunit.png)